#!/usr/bin/perl
use strict;
use warnings;
use utf8;

my $usage = "
summarise_scenario1.pl
Written by Severin Uebbing
Usage: summarise_scenario1.pl > <outfile.tsv>
  Folder must contain gene expression summaries produced using StringTie.";

my @lst = ("SRR1795535","SRR1795537","SRR1795539","SRR1795541","SRR1795543","SRR1795545",
	"SRR1795547","SRR1795549","SRR1795551","SRR1795553","SRR1795555","SRR1795557",
	"SRR1795559","SRR1795561","SRR1795563","SRR1795565","SRR1795567","SRR1795569",
	"SRR1795571","SRR1795573","SRR1795575","SRR1795577","SRR1795579","SRR1795581",
	"SRR1795583","SRR1795679","SRR1795681","SRR1795683","SRR1795685","SRR1795687",
	"SRR1795689","SRR1795691","SRR1795693","SRR1795695","SRR1795697","SRR2972862",
	"SRR1795701","SRR1795703","SRR1795705","SRR1795707","SRR1795709","SRR1795711",
	"SRR1795713","SRR2972865","SRR1795717","SRR1795719","SRR1795721","SRR1795723",
	"SRR1795725","SRR1795727");

my %dict = (
	"SRR1795535" => "00h_RepA",
	"SRR1795537" => "01h_RepA",
	"SRR1795539" => "02h_RepA",
	"SRR1795541" => "03h_RepA",
	"SRR1795543" => "04h_RepA",
	"SRR1795545" => "05h_RepA",
	"SRR1795547" => "06h_RepA",
	"SRR1795549" => "07h_RepA",
	"SRR1795551" => "08h_RepA",
	"SRR1795553" => "09h_RepA",
	"SRR1795555" => "10h_RepA",
	"SRR1795557" => "11h_RepA",
	"SRR1795559" => "12h_RepA",
	"SRR1795561" => "13h_RepA",
	"SRR1795563" => "14h_RepA",
	"SRR1795565" => "15h_RepA",
	"SRR1795567" => "16h_RepA",
	"SRR1795569" => "17h_RepA",
	"SRR1795571" => "18h_RepA",
	"SRR1795573" => "19h_RepA",
	"SRR1795575" => "20h_RepA",
	"SRR1795577" => "21h_RepA",
	"SRR1795579" => "22h_RepA",
	"SRR1795581" => "23h_RepA",
	"SRR1795583" => "24h_RepA",
	"SRR1795679" => "00h_RepB",
	"SRR1795681" => "01h_RepB",
	"SRR1795683" => "02h_RepB",
	"SRR1795685" => "03h_RepB",
	"SRR1795687" => "04h_RepB",
	"SRR1795689" => "05h_RepB",
	"SRR1795691" => "06h_RepB",
	"SRR1795693" => "07h_RepB",
	"SRR1795695" => "08h_RepB",
	"SRR1795697" => "09h_RepB",
	"SRR2972862" => "10h_RepB",
	"SRR1795701" => "11h_RepB",
	"SRR1795703" => "12h_RepB",
	"SRR1795705" => "13h_RepB",
	"SRR1795707" => "14h_RepB",
	"SRR1795709" => "15h_RepB",
	"SRR1795711" => "16h_RepB",
	"SRR1795713" => "17h_RepB",
	"SRR2972865" => "18h_RepB",
	"SRR1795717" => "19h_RepB",
	"SRR1795719" => "20h_RepB",
	"SRR1795721" => "21h_RepB",
	"SRR1795723" => "22h_RepB",
	"SRR1795725" => "23h_RepB",
	"SRR1795727" => "24h_RepB");

my %len = ();
open(IN, "SRR1795535.tsv");
while(<IN>){
	next if(/^Gene ID/);
	chomp;
	my @t = split;
	$len{$t[1]} = $t[5] - $t[4] +1;
}
close(IN);

my %cnt = ();
my %tpm = ();
my %fpkm = ();
foreach my $a (@lst){
	open(IN, $a."\.tsv");
	while(<IN>){
		next if(/^Gene ID/);
		chomp;
		my @t = split;
		my $cnt = $t[6] * $len{$t[1]};
		$cnt{$t[1]}{$a} = sprintf("%.0f", $cnt);
		$fpkm{$t[1]}{$a} = $t[7];
		$tpm{$t[1]}{$a} = $t[8];
	}
	close(IN);
}

print "Gene_name\tLength";
foreach my $b (@lst){
	print "\t$dict{$b}\_Cnt";
}
foreach my $b (@lst){
	print "\t$dict{$b}\_TPM";
}
foreach my $b (@lst){
	print "\t$dict{$b}\_FPKM";
}
print "\n";
foreach my $gene (sort keys %tpm){
	print "$gene\t$len{$gene}"; #gene_id gene_len
	for my $sample (@lst){
		print "\t$cnt{$gene}{$sample}";
	}
	for my $sample (@lst){
		print "\t$tpm{$gene}{$sample}";
	}
	for my $sample (@lst){
		print "\t$fpkm{$gene}{$sample}";
	}
	print "\n";
}
