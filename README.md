# README for https://bitbucket.org/severinevo/normalise/.
Written by Severin Uebbing.

## Data download and mapping

Sequencing data was donwloaded using the SRA toolkit's `$ fastq-dump --split-3 <accession>`.

Data was mapped using HISAT2. First, genome indeces were built: `$ hisat2-build <genome.fa> <index_name>`, with ERCC transcripts included as additional genome fasta entries for scenario 1.

Data was mapped:
```
$ hisat2 --dta -x <genome_index> [-U <single-end.fastq> | -1 <read1.fastq> -2 <read2.fastq>] -p 6 -S <mapped.sam>
$ samtools view -u <mapped.sam> | samtools sort - -o <mapped_sort.bam>
$ stringtie <mapped_sort.bam> -e -G <gene_annotations.gtf> -A <gene_abundance_output.tsv>
```

## Data summarisation and analysis

Gene abundance output files from StringTie were summarised using a Perl script:
```
$ perl summarise_scenario1.pl >scenario1_exp-summary.tsv
$ perl summarise_scenario2.pl
```
Data was analysed and figures were produced using R:
```
$ R CMD BATCH 180824_scenario1.R
$ R CMD BATCH 180824_scenario2.R
```
