#!/usr/bin/perl
use strict;
use warnings;
use utf8;

my $usage = "
summarise_scenario2.pl
Written by Severin Uebbing
Usage: summarise_scenario2.pl
  Folder must contain gene expression summaries produced using StringTie.";

my @gga = ("SRR306718","SRR306719-20","SRR924555","SRR924556");
my @hsa = ("ERR1331693","ERR1331713","SRR306854-5");
my @mml = ("ERR1331687","ERR1331689","SRR306786","SRR306787-8");
my @mmu = ("ERR1138638","SRR306772","SRR306773");

my @gga = ("SRR306718","SRR306719-20","SRR924555","SRR924556");
my @hsa = ("ERR1331693","ERR1331713","SRR306854-5");
my @mml = ("ERR1331687","ERR1331689","SRR306786","SRR306787-8");
my @mmu = ("ERR1138638","SRR306772","SRR306773");

my %dict = (
	"ERR1138638" => "mmu_lv_F2",
	"ERR1331687" => "mml_lv_F2",
	"ERR1331689" => "mml_lv_M2",
	"ERR1331693" => "hsa_lv_NA2",
	"ERR1331713" => "hsa_lv_NA3",
	"SRR306718" => "gga_lv_F1",
	"SRR306719-20" => "gga_lv_M1",
	"SRR306772" => "mmu_lv_F1",
	"SRR306773" => "mmu_lv_M1",
	"SRR306786" => "mml_lv_F1",
	"SRR306787-8" => "mml_lv_M1",
	"SRR306854-5" => "hsa_lv_M1",
	"SRR924555" => "gga_lv_M2",
	"SRR924556" => "gga_lv_F2");

my %gga_len = ();
open(IN, "SRR306718.tsv");
while(<IN>){
	next if(/^Gene ID/);
	chomp;
	my @t = split;
	$gga_len{$t[0]} = $t[5] - $t[4] +1;
}
close(IN);
my %hsa_len = ();
open(IN, "ERR1331693.tsv");
while(<IN>){
	next if(/^Gene ID/);
	chomp;
	my @t = split;
	$hsa_len{$t[0]} = $t[5] - $t[4] +1;
}
close(IN);
my %mml_len = ();
open(IN, "ERR1331687.tsv");
while(<IN>){
	next if(/^Gene ID/);
	chomp;
	my @t = split;
	$mml_len{$t[0]} = $t[5] - $t[4] +1;
}
close(IN);
my %mmu_len = ();
open(IN, "ERR1138638.tsv");
while(<IN>){
	next if(/^Gene ID/);
	chomp;
	my @t = split;
	$mmu_len{$t[0]} = $t[5] - $t[4] +1;
}
close(IN);

my %gga_cnt = ();
my %gga_tpm = ();
my %gga_fpkm = ();
foreach my $a (@gga){
	open(IN, $a."\.tsv");
	while(<IN>){
		next if(/^Gene ID/);
		chomp;
		my @t = split;
		my $gga_cnt = $t[6] * $gga_len{$t[0]};
		$gga_cnt{$t[0]}{$a} = sprintf("%.0f", $gga_cnt);
		$gga_fpkm{$t[0]}{$a} = $t[7];
		$gga_tpm{$t[0]}{$a} = $t[8];
	}
	close(IN);
}
my %hsa_cnt = ();
my %hsa_tpm = ();
my %hsa_fpkm = ();
foreach my $a (@hsa){
	open(IN, $a."\.tsv");
	while(<IN>){
		next if(/^Gene ID/);
		chomp;
		my @t = split;
		my $hsa_cnt = $t[6] * $hsa_len{$t[0]};
		$hsa_cnt{$t[0]}{$a} = sprintf("%.0f", $hsa_cnt);
		$hsa_fpkm{$t[0]}{$a} = $t[7];
		$hsa_tpm{$t[0]}{$a} = $t[8];
	}
	close(IN);
}
my %mml_cnt = ();
my %mml_tpm = ();
my %mml_fpkm = ();
foreach my $a (@mml){
	open(IN, $a."\.tsv");
	while(<IN>){
		next if(/^Gene ID/);
		chomp;
		my @t = split;
		my $mml_cnt = $t[6] * $mml_len{$t[0]};
		$mml_cnt{$t[0]}{$a} = sprintf("%.0f", $mml_cnt);
		$mml_fpkm{$t[0]}{$a} = $t[7];
		$mml_tpm{$t[0]}{$a} = $t[8];
	}
	close(IN);
}
my %mmu_cnt = ();
my %mmu_tpm = ();
my %mmu_fpkm = ();
foreach my $a (@mmu){
	open(IN, $a."\.tsv");
	while(<IN>){
		next if(/^Gene ID/);
		chomp;
		my @t = split;
		my $mmu_cnt = $t[6] * $mmu_len{$t[0]};
		$mmu_cnt{$t[0]}{$a} = sprintf("%.0f", $mmu_cnt);
		$mmu_fpkm{$t[0]}{$a} = $t[7];
		$mmu_tpm{$t[0]}{$a} = $t[8];
	}
	close(IN);
}

open(OUT, ">180611_gga-summary.tsv");
print OUT "Gene_ID\tLength";
foreach my $b (@gga){
	print OUT "\t$dict{$b}\_Cnt";
}
foreach my $b (@gga){
	print OUT "\t$dict{$b}\_TPM";
}
foreach my $b (@gga){
	print OUT "\t$dict{$b}\_FPKM";
}
print OUT "\n";
foreach my $gene (sort keys %gga_tpm){
	print OUT "$gene\t$gga_len{$gene}"; #gene_id gene_len
	for my $sample (@gga){
		print OUT "\t$gga_cnt{$gene}{$sample}";
	}
	for my $sample (@gga){
		print OUT "\t$gga_tpm{$gene}{$sample}";
	}
	for my $sample (@gga){
		print OUT "\t$gga_fpkm{$gene}{$sample}";
	}
	print OUT "\n";
}
close(OUT);
open(OUT, ">180611_hsa-summary.tsv");
print OUT "Gene_ID\tLength";
foreach my $b (@hsa){
	print OUT "\t$dict{$b}\_Cnt";
}
foreach my $b (@hsa){
	print OUT "\t$dict{$b}\_TPM";
}
foreach my $b (@hsa){
	print OUT "\t$dict{$b}\_FPKM";
}
print OUT "\n";
foreach my $gene (sort keys %hsa_tpm){
	print OUT "$gene\t$hsa_len{$gene}"; #gene_id gene_len
	for my $sample (@hsa){
		print OUT "\t$hsa_cnt{$gene}{$sample}";
	}
	for my $sample (@hsa){
		print OUT "\t$hsa_tpm{$gene}{$sample}";
	}
	for my $sample (@hsa){
		print OUT "\t$hsa_fpkm{$gene}{$sample}";
	}
	print OUT "\n";
}
close(OUT);
open(OUT, ">180611_mml-summary.tsv");
print OUT "Gene_ID\tLength";
foreach my $b (@mml){
	print OUT "\t$dict{$b}\_Cnt";
}
foreach my $b (@mml){
	print OUT "\t$dict{$b}\_TPM";
}
foreach my $b (@mml){
	print OUT "\t$dict{$b}\_FPKM";
}
print OUT "\n";
foreach my $gene (sort keys %mml_tpm){
	print OUT "$gene\t$mml_len{$gene}"; #gene_id gene_len
	for my $sample (@mml){
		print OUT "\t$mml_cnt{$gene}{$sample}";
	}
	for my $sample (@mml){
		print OUT "\t$mml_tpm{$gene}{$sample}";
	}
	for my $sample (@mml){
		print OUT "\t$mml_fpkm{$gene}{$sample}";
	}
	print OUT "\n";
}
close(OUT);
open(OUT, ">180611_mmu-summary.tsv");
print OUT "Gene_ID\tLength";
foreach my $b (@mmu){
	print OUT "\t$dict{$b}\_Cnt";
}
foreach my $b (@mmu){
	print OUT "\t$dict{$b}\_TPM";
}
foreach my $b (@mmu){
	print OUT "\t$dict{$b}\_FPKM";
}
print OUT "\n";
foreach my $gene (sort keys %mmu_tpm){
	print OUT "$gene\t$mmu_len{$gene}"; #gene_id gene_len
	for my $sample (@mmu){
		print OUT "\t$mmu_cnt{$gene}{$sample}";
	}
	for my $sample (@mmu){
		print OUT "\t$mmu_tpm{$gene}{$sample}";
	}
	for my $sample (@mmu){
		print OUT "\t$mmu_fpkm{$gene}{$sample}";
	}
	print OUT "\n";
}
close(OUT);
