mean_na<-function(x){mean(x,na.rm=T)}
sd_na<-function(x){sd(x,na.rm=T)}
quartile<-function(x){quantile(x,probs=c(.25,.75))}
z_fpkm<-function(i){
  if(all(i>0)) warning('Input not log2 transformed.')
  if(all(!is.na(i))) warning('0\'s need to be NA\'s.')
  my<-density(i,na.rm=T)$x[which.max(density(i,na.rm=T)$y)]
  U<-mean(i[i>my],na.rm=T)
  sigma<-(U-my)*(.5*pi)^.5
  z<-(i-my)/sigma
  z[z< -3]<-NA
  return(z)
}
